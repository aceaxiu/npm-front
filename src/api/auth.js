import WebApi from "./webApi";

class WebApiAuth {
  static login(data) {
    return WebApi.ApisType(`/api/auth/login`, "post", data);
  }

  static createUser(data) {
    return WebApi.ApisType(`/api/auth/sig-in`, "post", data);
  }

  static updateUser(userId, data) {
    return WebApi.ApisType(`/api/user/${userId}`, "put", data);
  }

  static getProfile() {
    return WebApi.ApisType(`/api/auth/me`, "get");
  }

  static getUsers(filter) {
    return WebApi.ApisType(`/api/users${filter}`, "get");
  }

  static deleteUser(id) {
    return WebApi.ApisType(`/api/user/${id}`, "delete");
  }
}

export default WebApiAuth;
