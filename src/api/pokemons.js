import WebApi from "./webApi";

class WebApiPokemons {
  static getPokemons(filter) {
    return WebApi.ApisType(`/api/pokemon/list?${filter}`, "get");
  }

  static getPokemonsFilter(name) {
    return WebApi.ApisType(`/api/pokemon/filter?name=${name}`, "get");
  }

  static getPokemonsUser(user) {
    return WebApi.ApisType(`/api/pokemon/user/${user}`, "get");
  }

  static addPokemonsUser(pokemons) {
    return WebApi.ApisType(`/api/pokemon`, "post", pokemons);
  }

  static updatePokemonsUser(pokemons) {
    return WebApi.ApisType(`/api/pokemon/list`, "put", pokemons);
  }
}

export default WebApiPokemons;
