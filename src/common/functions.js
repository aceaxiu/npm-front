export const extractFormData = (event) => {
  event.preventDefault();
  return Array.from(event.target.elements)
    .filter((input) => input.name)
    .reduce(
      (obj, input) => Object.assign(obj, { [input.name]: input.value }),
      {}
    );
};
