const InputForm = ({ placeholder, name, value = "" }) => {
  return (
    <input
      type="text"
      className="border-solid border-gray-400 border p-2 w-full rounded-lg"
      placeholder={placeholder}
      name={name}
      defaultValue={value}
    />
  );
};

export default InputForm;
