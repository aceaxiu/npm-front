const ContentLayout = ({ children }) => {
  return <div className="p-4">{children}</div>;
};

export default ContentLayout;
