import NavBar from "../NavBar";
import ContentLayout from "./Contentlayout";

const MainLayout = ({ children }) => {
  return (
    <div className="mainLayout">
      {/* <Alert /> */}
      <NavBar />
      <ContentLayout>{children}</ContentLayout>
    </div>
  );
};

export default MainLayout;
