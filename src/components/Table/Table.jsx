import Body from "./components/body";
import TableHeader from "./components/header";
import Pagination from "./components/pagination";

const GenericTable = ({ columns, dataSources, action }) => {
  return (
    <>
      <table className="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
        <TableHeader columns={columns} />
        <Body dataSource={dataSources} columns={columns} action={action} />
      </table>
      <Pagination data={dataSources} pageSize={5} />
    </>
  );
};

export default GenericTable;
