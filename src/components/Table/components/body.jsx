import { Trash } from "@phosphor-icons/react";

const Body = ({ dataSource = [], columns, action }) => {
  return (
    <tbody>
      {dataSource.map((dataSource, i) => {
        return (
          <tr
            key={i}
            className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600"
          >
            {columns.map((c, j) => {
              return (
                <td key={j} className="px-6 py-2">
                  {c.dataIndex == "actions" ? (
                    <button
                      className="p-2"
                      onClick={() => action(dataSource.id)}
                    >
                      <Trash color="yellow" size={22} />
                    </button>
                  ) : (
                    dataSource[c.dataIndex]
                  )}
                </td>
              );
            })}
          </tr>
        );
      })}
    </tbody>
  );
};

export default Body;
