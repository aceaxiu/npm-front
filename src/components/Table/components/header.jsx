const TableHeader = ({ columns }) => {
  return (
    <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
      <tr>
        {columns.map((column, i) => {
          return (
            <th key={i} className="px-6 py-3">
              {column.title}
            </th>
          );
        })}
      </tr>
    </thead>
  );
};

export default TableHeader;
