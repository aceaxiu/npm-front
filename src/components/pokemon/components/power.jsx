import { Star } from "@phosphor-icons/react";

const Power = ({ power = 5 }) => {
  const stars = [];

  // Loop 5 times to create the elements
  for (let i = 1; i <= power; i++) {
    stars.push(
      <Star keiy={i} size={16} weight="fill" className="text-yellow-300" />
    );
  }

  return (
    <div className="flex items-center mt-2.5 mb-5">
      <h1 className="font-semibold text-xs tracking-tight text-gray-900 dark:text-white mr-2">
        Poder
      </h1>
      <div className="flex items-center space-x-1 rtl:space-x-reverse">
        {stars}
      </div>
      <span className="bg-blue-100 text-blue-800 text-xs font-semibold px-2.5 py-0.5 rounded dark:bg-blue-200 dark:text-blue-800 ms-3">
        5.0
      </span>
    </div>
  );
};

export default Power;
