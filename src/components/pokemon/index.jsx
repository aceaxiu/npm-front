import { useRouter } from "next/router";
import Power from "./components/power";

const Pokemon = ({
  pokemon,
  action = "Elegir",
  viewButton = true,
  select,
  pokeSelected = [],
}) => {
  const router = useRouter();

  return (
    <>
      <div
        key={pokemon.id}
        className={`${
          pokeSelected.includes(pokemon.pokemon)
            ? " dark:bg-teal-500 dark:border-gray-700"
            : " dark:bg-gray-800 dark:border-gray-700"
        } w-full max-w-xs bg-white border border-gray-200 rounded-lg shadow`}
      >
        <div className="flex justify-center">
          <img
            className="p-8 min-h-72 max-h-72 rounded-t-lg"
            src={pokemon.image}
          />
        </div>
        <div className="px-5 pb-5">
          <div className="text-balance flex justify-center">
            <h3 className="text-3xl font-semibold tracking-tight text-gray-900 dark:text-white">
              {pokemon.pokemon}
            </h3>
          </div>
          <Power />
          {viewButton && (
            <div className="flex items-center justify-end">
              {pokeSelected.length < 6 ? (
                <button
                  onClick={() =>
                    action == "Cambiar"
                      ? router.push("/pokemons")
                      : select(pokemon.pokemon)
                  }
                  className={`${
                    pokeSelected.includes(pokemon.pokemon)
                      ? " bg-red-700 hover:bg-red-800"
                      : " bg-blue-700 hover:bg-blue-800"
                  } text-white focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center`}
                >
                  {pokeSelected.includes(pokemon.pokemon) ? "Cambiar" : action}
                </button>
              ) : (
                pokeSelected.includes(pokemon.pokemon) && (
                  <button
                    onClick={() =>
                      action == "Cambiar"
                        ? router.push("/pokemons")
                        : select(pokemon.pokemon)
                    }
                    className={`${
                      pokeSelected.includes(pokemon.pokemon)
                        ? " bg-red-700 hover:bg-red-800"
                        : " bg-blue-700 hover:bg-blue-800"
                    } text-white focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center`}
                  >
                    {pokeSelected.includes(pokemon.pokemon)
                      ? "Cambiar"
                      : action}
                  </button>
                )
              )}
            </div>
          )}
        </div>
      </div>
    </>
  );
};

export default Pokemon;
