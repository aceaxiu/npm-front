import cookie from "js-cookie";
import nextCookie from "next-cookies";
import Router from "next/router";
import { Component } from "react";

export const auth = async (ctx) => {
  const { token } = nextCookie(ctx);

  if (ctx.req && !token) {
    ctx.res.writeHead(302, { Location: "/" });
    ctx.res.end();
    return;
  }

  if (!token) {
    Router.push("/");
  }

  return token;
};

const setInitialProps = (component) => {
  component.getInitialProps = async (ctx) => {
    // We use `nextCookie` to get the cookie and pass the token to the
    // frontend in the `props`.
    const { token } = nextCookie(ctx);

    const redirectOnError = (msg = "") => {
      if (process.browser) {
        Router.push("/security/auth/login");
      } else {
        ctx.res.writeHead(302, { Location: "/security/auth/login" });
        ctx.res.end();
        return;
      }
    };
  };
};

// Gets the display name of a JSX component for dev tools
const getDisplayName = (Component) => {
  Component.displayName || Component.name || "Component";
};

// Aqui empieza todo el flujo de sesiones
export const withAuthSync = (WrappedComponent) =>
  class extends Component {
    static displayName = `withAuthSync(${getDisplayName(WrappedComponent)})`;

    static async getInitialProps(ctx) {
      setInitialProps(WrappedComponent);
      const token = auth(ctx);

      const componentProps =
        WrappedComponent.getInitialProps &&
        (await WrappedComponent.getInitialProps(ctx));
      return { ...componentProps, token };
    }

    render() {
      return <WrappedComponent {...this.props} />;
    }
  };

export const loginAuth = async (token) => {
  cookie.remove("token");
  cookie.set("token", token);
};

export const logoutAuth = async () => {
  cookie.remove("token");
  cookie.remove("user");
  Router.push("/");
};
