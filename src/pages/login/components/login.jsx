const LoginForm = ({ action, setRegister }) => {
  return (
    <>
      <form onSubmit={action}>
        <div className="mb-4">
          <input
            type="email"
            name="email"
            className="shadow-sm rounded-md w-full px-3 py-2 border border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500"
            placeholder="your@email.com"
            required
          />
        </div>
        <div className="mb-6">
          <input
            type="password"
            name="password"
            className="shadow-sm rounded-md w-full px-3 py-2 border border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500"
            placeholder="Password"
            required
          />
        </div>

        <button
          type="submit"
          className="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
        >
          Iniciar sesion
        </button>
      </form>
      <div className="flex justify-end text-white text-sm mt-4">
        No te has registrado?
        <button
          className="ml-4 text-blue-500"
          onClick={() => setRegister(true)}
        >
          Registrate
        </button>
      </div>
    </>
  );
};

export default LoginForm;
