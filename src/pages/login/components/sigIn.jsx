const SigInForm = ({ action, setRegister }) => {
  return (
    <>
      <form onSubmit={action}>
        <div className="mb-4">
          <input
            type="email"
            name="email"
            className="shadow-sm rounded-md w-full px-3 py-2 border border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500"
            placeholder="your@email.com"
            required
          />
        </div>
        <div className="mb-4">
          <input
            type="text"
            name="fullName"
            className="shadow-sm rounded-md w-full px-3 py-2 border border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500"
            placeholder="Nombre(s)"
            required
          />
        </div>
        <div className="mb-4">
          <input
            type="text"
            name="firstName"
            className="shadow-sm rounded-md w-full px-3 py-2 border border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500"
            placeholder="Primer apellido"
            required
          />
        </div>
        <div className="mb-4">
          <input
            type="text"
            name="lastName"
            className="shadow-sm rounded-md w-full px-3 py-2 border border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500"
            placeholder="Segundo apellido"
          />
        </div>
        <div className="mb-4">
          <input
            type="text"
            name="phone"
            className="shadow-sm rounded-md w-full px-3 py-2 border border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500"
            placeholder="Telefono"
            required
          />
        </div>
        <div className="mb-6">
          <input
            type="password"
            name="password"
            className="shadow-sm rounded-md w-full px-3 py-2 border border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500"
            placeholder="Password"
            required
          />
        </div>

        <button
          type="submit"
          className="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
        >
          Registrarse
        </button>
      </form>
      <button
        onClick={() => setRegister(false)}
        className="w-full flex justify-center py-2 px-4 mt-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
      >
        Regresar
      </button>
    </>
  );
};

export default SigInForm;
