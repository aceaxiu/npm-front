import WebApiAuth from "@/api/auth";
import { extractFormData } from "@/common/functions";
import { loginAuth } from "@/libs/auth";
import { useRouter } from "next/router";
import { useState } from "react";
import { connect } from "react-redux";
import { Login } from "../../redux/webDuck";
import LoginForm from "./components/login";
import SigInForm from "./components/sigIn";

const Home = ({ ...props }) => {
  const router = useRouter();
  const [register, setregister] = useState(false);

  const login = (event) => {
    const form = extractFormData(event);
    props
      .Login(form)
      .then((result) => {
        if (result) {
          loginAuth(result.access_token);
          if (result.user.isAdmin) router.push({ pathname: "/users" });
          else router.push({ pathname: "/profile" });
        } else {
          // setError(true);
        }
      })
      .catch((err) => {
        console.log("🚀 ~ login ~ err:", err);
      });
  };

  const sigIn = async (event) => {
    const form = extractFormData(event);
    try {
      const response = await WebApiAuth.createUser(form);
      setregister(false);
    } catch (error) {
      console.log("🚀 ~ sigIn ~ error:", error);
    }
  };

  return (
    <div className="min-h-screen flex items-center justify-center w-full bg-cover bg-no-repeat bg-[url('/images/bg.png')]">
      <div className="bg-white dark:bg-gray-900 shadow-md rounded-lg px-8 py-6 max-w-md">
        <h1 className="text-2xl font-bold text-center mb-4 dark:text-gray-200">
          {register ? "Registro" : "Login"}
        </h1>

        {register ? (
          <SigInForm action={sigIn} setRegister={setregister} />
        ) : (
          <LoginForm action={login} setRegister={setregister} />
        )}
      </div>
    </div>
  );
};

const mapState = (state) => {
  return {};
};

export default connect(mapState, { Login })(Home);
