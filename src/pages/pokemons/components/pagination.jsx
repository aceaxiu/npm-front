import { CaretCircleLeft, CaretCircleRight } from "@phosphor-icons/react";

const Pagination = ({ pagination, nextPage, previousPage }) => {
  return (
    <div className="flex justify-end">
      <button
        disabled={pagination?.previous != null ? false : true}
        onClick={previousPage}
      >
        <CaretCircleLeft
          size={42}
          color={`${pagination?.previous != null ? "black" : "gray"}`}
        />
      </button>
      <button
        disabled={pagination?.next != null ? false : true}
        onClick={nextPage}
      >
        <CaretCircleRight size={42} />
      </button>
    </div>
  );
};

export default Pagination;
