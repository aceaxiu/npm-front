import { ArrowClockwise } from "@phosphor-icons/react";

const Search = ({ filter, getPokemons }) => {
  const resetForm = () => {
    const input = document.getElementsByName("name");
    input[0].value = "";
    getPokemons();
  };

  return (
    <div className="flex justify-center mb-4">
      <form onSubmit={filter}>
        <div className="flex justify-center align-middle">
          <input
            name="name"
            className="block w-full h-10 ps-5 text-sm border border-gray-300 rounded-lg bg-gray-50 dark:placeholder-gray-400"
            placeholder="Buscar por nombre"
            required
          />
          <button
            type="submit"
            className="mx-4 text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
          >
            Buscar
          </button>
          <button
            type="button"
            onClick={resetForm}
            className="text-white h-10  focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
          >
            <ArrowClockwise />
          </button>
        </div>
      </form>
    </div>
  );
};

export default Search;
