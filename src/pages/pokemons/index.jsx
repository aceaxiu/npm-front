import WebApiPokemons from "@/api/pokemons";
import { extractFormData } from "@/common/functions";
import MainLayout from "@/components/Layout/MainLayout";
import Pokemon from "@/components/pokemon";
import { withAuthSync } from "@/libs/auth";
import { useEffect, useState } from "react";
import { connect } from "react-redux";
import Pagination from "./components/pagination";
import Search from "./components/search";

const Pokemons = ({ ...props }) => {
  const [pokemons, setPokemons] = useState([]);
  const [pagination, setPagination] = useState(null);
  const [pokeSelected, setPokeSelected] = useState([]);
  const [pokedex, setPokedex] = useState([]);

  useEffect(() => {
    getPokemons();
  }, []);

  useEffect(() => {
    if (props.user) getPokemonsUser(props.user.id);
  }, [props.user]);

  const nextPage = () => {
    getPokemons(pagination.next);
  };

  const previousPage = () => {
    getPokemons(pagination.previous);
  };

  const getPokemons = async (filter = "offset=0&limit=11") => {
    try {
      const response = await WebApiPokemons.getPokemons(filter);
      const { pokemons, ...pagination } = response.data;
      setPokemons(pokemons);
      setPagination(pagination);
    } catch (error) {
      console.log("🚀 ~ getPokemons ~ error:", error);
    }
  };

  const getPokemonsUser = async (id) => {
    try {
      const response = await WebApiPokemons.getPokemonsUser(id);
      setPokeSelected(
        response.data.map((p) => {
          return p.pokemon;
        })
      );
      setPokedex(
        response.data.map((p) => {
          return p.id;
        })
      );
    } catch (error) {
      console.log("🚀 ~ pokemons ~ error:", error);
    }
  };

  const filterPokemon = async (data) => {
    try {
      const form = extractFormData(data);
      const response = await WebApiPokemons.getPokemonsFilter(form.name);
      setPokemons([response.data]);
    } catch (error) {
      console.log("🚀 ~ filterPokemon ~ error:", error);
    }
  };

  const addPokemons = async () => {
    try {
      if (pokedex.length > 0)
        await WebApiPokemons.updatePokemonsUser({
          userId: props.user.id,
          pokemons: pokeSelected,
          oldPokemons: pokedex,
        });
      else
        await WebApiPokemons.addPokemonsUser({
          userId: props.user.id,
          pokemons: pokeSelected,
        });
      getPokemonsUser(props.user.id);
    } catch (error) {
      console.log("🚀 ~ addPokemons ~ error:", error);
    }
  };

  const selectPokemons = (id) => {
    if (pokeSelected.includes(id)) {
      setPokeSelected([...pokeSelected.filter((p) => p != id)]);
    } else setPokeSelected([...pokeSelected, id]);
  };

  const PokemonsList = ({ data = [] }) => {
    return data.map((pokemon, i) => {
      return (
        <Pokemon
          key={pokemon.id + i}
          pokemon={pokemon}
          viewButton={!props.user.isAdmin}
          select={selectPokemons}
          pokeSelected={pokeSelected}
        />
      );
    });
  };

  return (
    <MainLayout>
      <Search filter={filterPokemon} getPokemons={getPokemons} />{" "}
      <div className="flex justify-center w-full text-xl p-4 ">
        Solo puedes elegir un maximo de 6 pokemons
      </div>
      <div className="flex justify-center w-full text-xl p-4 ">
        <button
          onClick={addPokemons}
          className="text-white focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center bg-green-700"
        >
          Guardar
        </button>
      </div>
      {pokemons.length > 0 ? (
        <>
          {pokemons.length > 1 && (
            <Pagination
              pagination={pagination}
              nextPage={nextPage}
              previousPage={previousPage}
            />
          )}
          <div className="grid grid-cols-1 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-6 gap-2">
            <PokemonsList data={pokemons} />
          </div>
        </>
      ) : (
        <div className="flex justify-center w-full text-xl p-10 ">
          No se encontraron resultados
        </div>
      )}
    </MainLayout>
  );
};

const mapState = (state) => {
  return {
    user: state.globalStore.user,
  };
};

export default connect(mapState)(withAuthSync(Pokemons));
