import WebApiAuth from "@/api/auth";
import { extractFormData } from "@/common/functions";
import InputForm from "@/components/Input";

const Form = ({ user = null, setProfile }) => {
  const sendData = async (data) => {
    try {
      const form = extractFormData(data);
      const response = await WebApiAuth.updateUser(user.id, form);
      setProfile(response.data);
    } catch (error) {
      console.log("🚀 ~ sendData ~ error:", error);
    }
  };

  return (
    <>
      <div className="bg-white w-full rounded-lg">
        <div className="text-3xl mb-6 text-left font-semibold">
          Datos generales
        </div>
        <form onSubmit={sendData}>
          <div className="grid grid-cols-1 lg:grid-cols-4 gap-4 m-auto">
            <div className="col-span-8 lg:col-span-1">
              <InputForm
                name="fullName"
                placeholder="Nombre"
                value={user?.fullName}
              />
            </div>

            <div className="col-span-8 lg:col-span-1">
              <InputForm
                name="firstName"
                placeholder="Primer apellido"
                value={user?.firstName}
              />
            </div>

            <div className="col-span-8 lg:col-span-1">
              <InputForm
                name="lastName"
                placeholder="Segundo Apellido"
                value={user?.lastName}
              />
            </div>

            <div className="col-span-8 lg:col-span-1">
              <InputForm
                name="phone"
                placeholder="Telefono"
                value={user?.phone}
              />
            </div>
          </div>
          <div className="flex text-right justify-end mt-6 space-x-4">
            <button
              type="submit"
              className="py-3 px-6 bg-green-600 text-white font-bold w-full rounded-lg sm:w-32"
            >
              Guardar
            </button>
          </div>
        </form>
      </div>
    </>
  );
};

export default Form;
