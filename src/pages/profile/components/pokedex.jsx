import Pokemon from "@/components/pokemon";

const Pokedex = ({ pokemons = [] }) => {
  const Pokemons = ({ data }) => {
    return data.map((p, i) => {
      return <Pokemon key={i} pokemon={p} action={"Cambiar"} />;
    });
  };

  return (
    <>
      <div className="text-3xl mb-6 text-left font-semibold">Pokedex</div>
      <div className="grid grid-cols-1 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-6 gap-2">
        <Pokemons data={pokemons} />
      </div>
    </>
  );
};

export default Pokedex;
