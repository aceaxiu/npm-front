import WebApiAuth from "@/api/auth";
import WebApiPokemons from "@/api/pokemons";
import MainLayout from "@/components/Layout/MainLayout";
import { withAuthSync } from "@/libs/auth";
import { useEffect, useState } from "react";
import { connect } from "react-redux";
import Form from "./components/form";
import Pokedex from "./components/pokedex";

const Profile = ({ ...props }) => {
  const [profile, setProfile] = useState([]);
  const [pokemons, setPokemons] = useState([]);

  useEffect(() => {
    if (props.user) getProfile();
  }, [props.user]);

  const getProfile = async () => {
    try {
      const response = await WebApiAuth.getProfile(props.user.id);
      setProfile(response.data);
      getPokemons(response.data.id);
    } catch (error) {
      console.log("🚀 ~ getProfile ~ error:", error);
    }
  };

  const getPokemons = async (id) => {
    try {
      const response = await WebApiPokemons.getPokemonsUser(id);
      setPokemons(response.data);
    } catch (error) {
      console.log("🚀 ~ pokemons ~ error:", error);
    }
  };

  return (
    <MainLayout>
      <Form user={profile} setProfile={setProfile} />
      <Pokedex pokemons={pokemons} />
    </MainLayout>
  );
};

const mapState = (state) => {
  return {
    user: state.globalStore.user,
  };
};

export default connect(mapState)(withAuthSync(Profile));
