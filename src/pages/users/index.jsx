import WebApiAuth from "@/api/auth";
import MainLayout from "@/components/Layout/MainLayout";
import GenericTable from "@/components/Table/Table";
import { useEffect, useState } from "react";

const Users = () => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    getUsers();
  }, []);

  const columns = [
    { title: "Nombre", dataIndex: "fullName" },
    { title: "Email", dataIndex: "email" },
    { title: "Telefono", dataIndex: "phone" },
    {
      title: "Acciones",
      dataIndex: "actions",
    },
  ];

  const getUsers = async (filter = "") => {
    try {
      const response = await WebApiAuth.getUsers(filter);
      setUsers(response.data);
    } catch (error) {
      console.log("🚀 ~ getUsers ~ error:", error);
    }
  };

  const removeUser = async (id) => {
    try {
      await WebApiAuth.deleteUser(id);
      setUsers(users.filter((user) => user.id != id));
    } catch (error) {
      console.log("🚀 ~ getUsers ~ error:", error);
    }
  };

  return (
    <MainLayout>
      <GenericTable columns={columns} dataSources={users} action={removeUser} />
    </MainLayout>
  );
};

export default Users;
