import { combineReducers, configureStore } from "@reduxjs/toolkit";
import webReducer, { validatedLogin } from "./webDuck";

const rootReducer = combineReducers({
  globalStore: webReducer,
});

export const store = configureStore({
  reducer: rootReducer,
});

export default () => {
  validatedLogin()(store.dispatch);
  return store;
};
