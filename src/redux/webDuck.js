import Cookies from "js-cookie";
import WebApiAuth from "../api/auth";

const initialData = {
  user: null,
};

const USER = "USER";
const webReducer = (state = initialData, action) => {
  switch (action.type) {
    case USER:
      return { ...state, user: action.payload };
    default:
      return state;
  }
};
export default webReducer;

export const validatedLogin = () => async (dispatch) => {
  try {
    let session = Cookies.get("user");
    let token = Cookies.get("token");
    if (session && token) {
      session = JSON.parse(session);
      dispatch({ type: USER, payload: session });
      return session;
    } else {
      Cookies.remove("session");
      Cookies.remove("token");
    }
  } catch (error) {
    return false;
  }
};

export const Login = (data) => async (dispatch) => {
  try {
    const login = await WebApiAuth.login(data);
    dispatch({ type: USER, payload: login.data.user });
    Cookies.set("user", JSON.stringify(login.data.user));
    Cookies.set("token", login.data.access_token);
    return login.data;
  } catch (error) {
    return false;
  }
};
